#include "MiniGame.h"

#include <cassert>
#include <cstdlib>
#include <ctime>

void DrawRect(const SDL_Rect& screenCoords, int textureId, const SDL_Rect& textureCoords);
void DrawRect(const SDL_Rect& rect);
void DrawFillRect(const SDL_Rect& rect);

MiniGame::MiniGame(int width, int height)
		: mWidth(width)
		, mHeight(height)
		, mIsSelected(false)
		, mIsEasing(false) {
	assert(width > 0 && height > 0 && cCols > 1 && cRows > 1);
	const int w = mWidth / cCols;
	const int h = mHeight / cRows;
	for (int row = 0, y = 0, id = 0; row < cRows; ++row, y += h) {
		for (int col = 0, x = 0; col < cCols; ++col, x += w, ++id) {
			mGrid[row][col].screenCoords.x = x;
			mGrid[row][col].screenCoords.y = y;
			mGrid[row][col].screenCoords.w = w;
			mGrid[row][col].screenCoords.h = h;
			mGrid[row][col].textureCoords.x = x;
			mGrid[row][col].textureCoords.y = y;
			mGrid[row][col].textureCoords.w = w;
			mGrid[row][col].textureCoords.h = h;
			mGrid[row][col].id = id;
		}
	}
}

void MiniGame::Initialize() {
	srand(static_cast<unsigned>(time(0)));
	Restart();
}

void MiniGame::Click(float x, float y) {
	if (IsCompleted() || mIsEasing) {
		return;
	}

	if (mIsSelected) {
		const int row = GetRow(y);
		const int col = GetCol(x);
		if (row != mFirstSelectedRow || col != mFirstSelectedCol) {
			mIsSelected = false;
			mIsEasing = true;
			mSecondSelectedRow = row;
			mSecondSelectedCol = col;
			mFirstTween.SetInitial(Vector(mGrid[mFirstSelectedRow][mFirstSelectedCol].screenCoords.x, mGrid[mFirstSelectedRow][mFirstSelectedCol].screenCoords.y));
			mFirstTween.SetEnd(Vector(mGrid[mSecondSelectedRow][mSecondSelectedCol].screenCoords.x, mGrid[mSecondSelectedRow][mSecondSelectedCol].screenCoords.y));
			mFirstTween.SetTime(0);
			mFirstTween.Resume();
			mSecondTween.SetInitial(Vector(mGrid[mSecondSelectedRow][mSecondSelectedCol].screenCoords.x, mGrid[mSecondSelectedRow][mSecondSelectedCol].screenCoords.y));
			mSecondTween.SetEnd(Vector(mGrid[mFirstSelectedRow][mFirstSelectedCol].screenCoords.x, mGrid[mFirstSelectedRow][mFirstSelectedCol].screenCoords.y));
			mSecondTween.SetTime(0);
			mSecondTween.Resume();
		}
	}
	else {
		mIsSelected = true;
		mFirstSelectedRow = GetRow(y);
		mFirstSelectedCol = GetCol(x);
	}
}

bool MiniGame::IsCompleted() const {
	for (int row = 0, id = 0; row < cRows; ++row) {
		for (int col = 0; col < cCols; ++col, ++id) {
			if (mGrid[row][col].id != id) {
				return false;
			}
		}
	}
	return true;
}

void MiniGame::Render() const {
	for (int row = 0; row < cRows; ++row) {
		for (int col = 0; col < cCols; ++col) {
			DrawRect(mGrid[row][col].screenCoords, cTextureId, mGrid[row][col].textureCoords);
		}
	}
	if (mIsSelected) {
		DrawRect(mGrid[mFirstSelectedRow][mFirstSelectedCol].screenCoords);
	}
	if (mIsEasing) {
		DrawFillRect(mGrid[mFirstSelectedRow][mFirstSelectedCol].screenCoords);
		DrawFillRect(mGrid[mSecondSelectedRow][mSecondSelectedCol].screenCoords);
		DrawTweenRect(mFirstSelectedRow, mFirstSelectedCol, static_cast<int>(mFirstTween.Value().x), static_cast<int>(mFirstTween.Value().y));
		DrawTweenRect(mSecondSelectedRow, mSecondSelectedCol, static_cast<int>(mSecondTween.Value().x), static_cast<int>(mSecondTween.Value().y));
	}
}

void MiniGame::Restart() {
	mIsSelected = false;
	mIsEasing = false;
	do {
		for (int row = 0; row < cRows; ++row) {
			for (int col = 0; col < cCols; ++col) {
				Swap(row, col, NextInt(0, cRows - 1), NextInt(0, cCols - 1));
			}
		}
	} while (IsCompleted());
}

void MiniGame::Update(float deltaTime) {
	assert(deltaTime >= 0);
	if (mIsEasing) {
		mFirstTween.Update(deltaTime);
		mSecondTween.Update(deltaTime);
		if (mFirstTween.IsFinished()) {
			Swap(mFirstSelectedRow, mFirstSelectedCol, mSecondSelectedRow, mSecondSelectedCol);
			mIsEasing = false;
		}
	}
}

void MiniGame::Swap(int fromRow, int fromCol, int toRow, int toCol) {
	Cell tmp(mGrid[fromRow][fromCol]);
	mGrid[fromRow][fromCol].textureCoords = mGrid[toRow][toCol].textureCoords;
	mGrid[fromRow][fromCol].id = mGrid[toRow][toCol].id;
	mGrid[toRow][toCol].textureCoords = tmp.textureCoords;
	mGrid[toRow][toCol].id = tmp.id;
}

int MiniGame::NextInt(int min, int max) const {
    assert(min <= max);
    return rand() % (max - min + 1) + min;
}

int MiniGame::GetRow(float y) const {
	return (y == mHeight) ? cRows - 1 : static_cast<int>(y * cRows / mHeight);
}

int MiniGame::GetCol(float x) const {
	return (x == mWidth) ? cCols - 1 : static_cast<int>(x * cCols / mWidth);
}

void MiniGame::DrawTweenRect(int row, int col, int x, int y) const {
	SDL_Rect screenCoords = mGrid[row][col].screenCoords;
	screenCoords.x = x;
	screenCoords.y = y;
	DrawRect(screenCoords, cTextureId, mGrid[row][col].textureCoords);
}
