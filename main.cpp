#include <ctime>
#include <map>
#include <string>
#include <SDL.h>
#include <SDL_image.h>
#include "MiniGame.h"

SDL_Renderer* renderer = NULL;
typedef std::map<int, SDL_Texture*> Textures;
Textures textures;

bool LoadTexture(const char* path, int id) {
	SDL_Surface* surface = IMG_Load(path);
	if (surface == NULL) {
		return false;
	}

	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);
	if (texture == NULL) {
		return false;
	}

	textures[id] = texture;
	return true;
}

void DrawRect(const SDL_Rect& screenCoords, int textureId, const SDL_Rect& textureCoords) {
	SDL_RenderCopy(renderer, textures[textureId], &textureCoords, &screenCoords);
}

void DrawRect(const SDL_Rect& rect) {
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	SDL_RenderDrawRect(renderer, &rect);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
}

void DrawFillRect(const SDL_Rect& rect) {
	SDL_RenderFillRect(renderer, &rect);
}

int main(int argc, char* argv[]) {
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		return 1;
	}

	const int width = 640;
	const int height = 480;
	SDL_Window* window = SDL_CreateWindow("Puzzle", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN);
	if (window == NULL) {
		SDL_Quit();
		return 1;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == NULL) {
		SDL_DestroyWindow(window);
		SDL_Quit();
		return 1;
	}

	if (LoadTexture("assets/background1.jpg", 1) && LoadTexture("assets/background2.jpg", 2)) {
		MiniGame game(width, height);
		game.Initialize();

		clock_t time = clock();
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_Event event;
		while (true) {
			if (SDL_PollEvent(&event)) {
				if (event.type == SDL_QUIT) {
					break;
				}
				else if (event.type == SDL_MOUSEBUTTONUP) {
					if (event.button.button == SDL_BUTTON_LEFT) {
						game.Click(event.button.x, event.button.y);
					}
					else if (event.button.button == SDL_BUTTON_RIGHT) {
						game.Restart();
					}
				}
			}

			const clock_t now = clock();
    		const float deltaTime = static_cast<float>((now - time)) / CLOCKS_PER_SEC;
    		game.Update(deltaTime);
    		time = now;

			SDL_RenderClear(renderer);
			game.Render();
			SDL_RenderPresent(renderer);
		}
	}

	for (Textures::iterator iter = textures.begin(); iter != textures.end(); ++iter) {
		SDL_DestroyTexture(iter->second);
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
