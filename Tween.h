#ifndef TWEEN_H
#define TWEEN_H

#include <cassert>
#include <cmath>
#include <limits>

static float EaseInOutQuad(float t) {
	assert(t >= 0 && t <= 1);
	t += t;
	if (t < 1) {
		return t * t / 2;
	}
	else {
		--t;
		return -0.5f * (t * (t - 2) - 1);
	}
}

template<typename T>
class Tween {
private:
	enum TweenState {
		STATE_TWEENING,
		STATE_PAUSED,
		STATE_FINISHED
	};

public:
	Tween(const T& initial = T(), const T& end = T(), float duration = 0.65)
			: mValue(initial)
			, mState(STATE_FINISHED) {
		SetInitial(initial);
		SetEnd(end);
		SetDuration(duration);
		SetTime(0);
		Resume();
	}

	const T& Value() const {
		return mValue;
	}

	void SetInitial(const T& initial) {
		mChange = mChange + mInitial - initial;
		mInitial = initial;
	}

	void SetEnd(const T& end) {
		mChange = end - mInitial;
	}

	void SetDuration(float duration) {
		assert(duration > 0);
		mDuration = (duration > 0) ? duration : std::numeric_limits<float>::min();
	}

	void SetTime(float time) {
		assert(time >= 0 && time <= mDuration);
		mTime = (time < 0) ? 0 : ((time > mDuration) ? mDuration : time);
		if (mTime < mDuration && mState == STATE_FINISHED) {
			mState = STATE_PAUSED;
		}
	}

	void Resume() {
		assert(mState != STATE_FINISHED);
		if (mState != STATE_FINISHED) {
			mState = STATE_TWEENING;
			Update(0);
		}
	}

	void Pause() {
		assert(mState != STATE_FINISHED);
		if (mState != STATE_FINISHED) {
			mState = STATE_PAUSED;
		}
	}

	bool Update(float deltaTime) {
		assert(deltaTime >= 0);
		if (mState != STATE_TWEENING) {
			return false;
		}

		mTime += deltaTime;
		if (mTime < 0) {
			mTime = 0;
			mState = STATE_PAUSED;
		}
		else if (mTime >= mDuration) {
			mTime = mDuration;
			mState = STATE_FINISHED;
		}

		mValue = mInitial + mChange * EaseInOutQuad(mTime / mDuration);
		return true;
	}

	bool IsFinished() const {
		return mState == STATE_FINISHED;
	}

private:
	T mValue;
	T mInitial;
	T mChange;
	float mDuration;
	float mTime;
	TweenState mState;
};

#endif /* TWEEN_H */
