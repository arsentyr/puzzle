#ifndef MINI_GAME_H
#define MINI_GAME_H

#include <SDL.h>
#include "Tween.h"

class Vector {
public:
	float x;
	float y;

public:
	Vector(float x = 0, float y = 0) : x(x), y(y) {}
	
	Vector operator+(const Vector& vec) const { return Vector(x + vec.x, y + vec.y); }
	
	Vector operator-(const Vector& vec) const { return Vector(x - vec.x, y - vec.y); }
	
	Vector operator*(float scalar) const { return Vector(x * scalar, y * scalar); }
};

class MiniGame {
public:
	MiniGame(int width, int height);

	void Initialize();

	void Click(float x, float y);

	bool IsCompleted() const;

	void Render() const;

	void Restart();

	void Update(float deltaTime);

public:
	static const int cTextureId = 1;
	static const int cRows = 5;
	static const int cCols = 5;

private:
	void Swap(int fromRow, int fromCol, int toRow, int toCol);

	int NextInt(int min, int max) const;

	int GetRow(float y) const;

	int GetCol(float x) const;

	void DrawTweenRect(int row, int col, int x, int y) const;

private:
	struct Cell {
		SDL_Rect screenCoords;
		SDL_Rect textureCoords;
		int id; 
	};

	Cell mGrid[cRows][cCols];
	int mWidth;
	int mHeight;
	bool mIsSelected;
	int mFirstSelectedRow;
	int mFirstSelectedCol;
	int mSecondSelectedRow;
	int mSecondSelectedCol;
	bool mIsEasing;
	Tween<Vector> mFirstTween;
	Tween<Vector> mSecondTween;
};

#endif /* MINI_GAME_H */
